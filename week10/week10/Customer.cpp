#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::~Customer()
{
	this->_items.clear();
}

double Customer::totalSum() const
{
	int sum = 0;
	std::set<Item>::iterator i;
	for (i = this->_items.begin(); i != this->_items.end(); ++i)
	{
		sum += i->totalPrice();
	}

	return sum;
}

void Customer::addItem(Item item)
{
	int save_count = 0;
	std::set<Item>::iterator i;
	//will check if the item is exists. if exists will only add in the count
	for (i = this->_items.begin(); i != this->_items.end(); ++i)
	{
		if (i->operator==(item))
		{
			save_count = i->get_count() + 1;
			this->_items.erase(i);
			item.set_count(save_count);
			break;
		}
	}
	this->_items.insert(item);
}

void Customer::removeItem(Item item)
{
	int save_count = 0;
	std::set<Item>::iterator i;
	//will check if the item is exists. if exists will only add in the count
	for (i = this->_items.begin(); i != this->_items.end(); ++i)
	{
		if (i->operator==(item))
		{
			//if the count is more than 1 need only Subtract in one
			if (i->get_count() > 1)
			{	
				save_count = i->get_count() - 1;
				this->_items.erase(i);
				item.set_count(save_count);
				this->_items.insert(item);
				break;
			}
			else
			{
				this->_items.erase(i);
				break;
			}
		}
	}
}

std::set<Item> Customer::get_items()
{
	return this->_items;
}

std::string Customer::get_name()
{
	return this->_name;
}

void Customer::set_items(std::set<Item> items)
{
	this->_items = items;
}

void Customer::set_name(std::string name)
{
	this->_name = name;
}
