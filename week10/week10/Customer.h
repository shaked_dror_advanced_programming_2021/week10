#pragma once
#include"Item.h"
#include <iostream>
#include <vector>
#include<set>

class Customer
{
public:
	Customer(std::string name);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set

	//get and set functions
	std::set<Item> get_items();
	std::string get_name();

	void set_items(std::set<Item> items);
	void set_name(std::string name);

private:
	std::string _name;
	std::set<Item> _items;


};
