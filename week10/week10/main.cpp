#include"Customer.h"
#include<map>

#define ITEM_LIST_SIZE sizeof(itemList) / sizeof(itemList[0])

Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

void menu();
void check_cust();
void path1(std::map<std::string, Customer>& abcCustomers);
void path2(std::map<std::string, Customer>& abcCustomers);
void path3(std::map<std::string, Customer>& abcCustomers);
void print_user_items(std::map<std::string, Customer>&  abcCustomers, std::string user);
std::string input();


int main()
{
	menu();
	return 0;
}

void menu()
{
	std::map<std::string, Customer> abcCustomers;
	std::cout << "Welcome to Madshimart!\n" <<
		"1. to sign as customer and buy items\n" <<
		"2. to uptade existing customer's items\n" <<
		"3. to print the customer who pays the most\n" <<
		"4. to exit" << std::endl;
	std::string path = "0";
	while (path != "4")
	{
		try
		{
			std::cout << "put your path: " << std::endl;
			path = input();
			if (path == "1")
			{
				path1(abcCustomers);
			}
			else if (path == "2")
			{
				path2(abcCustomers);
			}
			else if (path == "3")
			{
				path3(abcCustomers);
			}

		}
		catch (const std::string& errorString)
		{
			std::cerr << "ERROR: " << errorString;
		}
	}

}

std::string input()
{
	std::string ans = "0";
	std::cin >> ans;
	
	while (ans < "1" || ans > "4")
	{
		std::cout << "wrong input! put input between 1 to 4" << std::endl;
		std::cin >> ans;
	}

	return ans;
}

void path1(std::map<std::string, Customer> &abcCustomers)
{
	std::cout << "put user name" << std::endl;
	std::string user;
	std::cin >> user;
	
	if (abcCustomers.find(user) == abcCustomers.end()) // not found
	{
	}
	else // found
	{	
		throw (std::string("the user is already exists\n"));
	}

	//adding user
	std::pair<std::string, Customer> cust(user, Customer(user));
	abcCustomers.insert(cust);

	std::cout << "The items you can buy are: (0 for exit)" << std::endl;
	for (int i = 0; i < 10; i++)
	{
		std::cout << "price: "<< itemList[i].get_unitPrice() <<"      "<< i+1<<".      "<<itemList[i].get_name() << std::endl;
	}

	std::string choice;
	do
	{
		std::cout << "put your choice: 0-10 --> ";
		std::cin >> choice;
	} while (choice < "0" || (choice.length() == 2 && choice[0] > '1' && choice[1] >'0') || (choice.length() == 1 && choice > "9"));

	while (choice != "0")
	{
		abcCustomers.find(user)->second.addItem(itemList[stoi(choice) - 1]);
		do
		{
			std::cout << "put your choice: 0-10 --> ";
			std::cin >> choice;
		} while (choice < "0" || (choice.length() == 2 && choice[0] > '1' && choice[1] > '0') || (choice.length() == 1 && choice > "9"));
	}
}

void path2(std::map<std::string, Customer>& abcCustomers)
{
	std::cout << "put user name" << std::endl;
	std::string user;
	std::cin >> user;

	if (abcCustomers.find(user) == abcCustomers.end()) // not found
	{
		throw (std::string("the user is not exists\n"));
	}
	//found
	std::string path_2;
	do
	{
		std::cout << "put your choice: 1-3 --> ";
		std::cin >> path_2;
	} while (path_2 < "1" || path_2 > "3");

	if (path_2 == "1")
	{
		std::cout << "The items you can buy are: (0 for exit)" << std::endl;
		for (int i = 0; i < 10; i++)
		{
			std::cout << "price: " << itemList[i].get_unitPrice() << "      " << i + 1 << ".      " << itemList[i].get_name() << std::endl;
		}

		std::string choice;
		do
		{
			std::cout << "put your choice: 0-10 --> ";
			std::cin >> choice;
		} while (choice < "0" || (choice.length() == 2 && choice[0] > '1' && choice[1] > '0') || (choice.length() == 1 && choice > "9"));

		while (choice != "0")
		{
			abcCustomers.find(user)->second.addItem(itemList[stoi(choice) - 1]);
			do
			{
				std::cout << "put your choice: 0-10 --> ";
				std::cin >> choice;
			} while (choice < "0" || (choice.length() == 2 && choice[0] > '1' && choice[1] > '0') || (choice.length() == 1 && choice > "9"));
		}
	}
	else if (path_2 == "2")
	{
		std::cout << "The items you can remove are: (0 for exit)" << std::endl;
		print_user_items(abcCustomers, user);

		std::string choice;
		do
		{
			std::cout << "put your choice: 0-10 --> ";
			std::cin >> choice;
		} while (choice < "0" || (choice.length() == 2 && choice[0] > '1' && choice[1] > '0') || (choice.length() == 1 && choice > "9"));

		while (choice != "0")
		{
			abcCustomers.find(user)->second.removeItem(itemList[stoi(choice) - 1]);
			do
			{
				std::cout << "put your choice: 0-10 --> ";
				std::cin >> choice;
			} while (choice < "0" || (choice.length() == 2 && choice[0] > '1' && choice[1] > '0') || (choice.length() == 1 && choice > "9"));
		}
	}
}

void path3(std::map<std::string, Customer>& abcCustomers)
{
	int max = 0;//0 is the minimum
	std::map<std::string, Customer>::iterator i;
	std::map<std::string, Customer>::iterator save;
	for (i = abcCustomers.begin(); i != abcCustomers.end(); ++i)
	{
		if (i->second.totalSum() > max)
		{
			save = i;
			max = i->second.totalSum();
		}
	}
	std::cout << "the user name is : " << save->first << ", the user total is: " << save->second.totalSum() << std::endl;
	std::cout << "the user items are: " << std::endl;
	print_user_items(abcCustomers, save->first);
}

void print_user_items(std::map<std::string, Customer>& abcCustomers, std::string user)
{	
	std::set<Item> temp = abcCustomers.find(user)->second.get_items();
	std::set<Item>::iterator i;
	for (i = temp.begin(); i != temp.end(); ++i)
	{
		for (int j = 0; j < 10; j++)
		{
			if (i->operator==(itemList[j]))
			{
				std::cout << "price: " << itemList[j].get_unitPrice() << "      " << j + 1 << ".      " << itemList[j].get_name() << std::endl;
			}
		}
	}
}

void check_cust()
{
	Customer cust = Customer("Shaked");
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("bread","00003",8.9),
		Item("Milk","00001",5.3),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

	//insert
	for (int i = 0; i < ITEM_LIST_SIZE; i++)
	{
		cust.addItem(itemList[i]);
	}
	cust.removeItem(Item("rice", "00006", 6.2));
	std::set<Item> Mylist = cust.get_items();
	std::set<Item>::iterator currItem;
	//print
	for (currItem = Mylist.begin(); currItem != Mylist.end(); ++currItem)
	{
		std::cout << currItem->get_name() << std::endl;
	}
}