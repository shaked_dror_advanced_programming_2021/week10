#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	_count = 1;
	_name = name;

	//_serialNumber = serialNumber;
	this->set_serialNumber(serialNumber);
	
	//_unitPrice = unitPrice;
	this->set_unitPrice(unitPrice);	
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

bool Item::operator<(const Item& other) const
{
	if (this->_serialNumber < other._serialNumber)
	{
		return true;
	}
	return false;
}

bool Item::operator==(const Item& other) const
{
	if (this->_serialNumber == other._serialNumber)
	{
		return true;
	}
	return false;
}

bool Item::operator>(const Item& other) const
{
	if (this->_serialNumber > other._serialNumber)
	{
		return true;
	}
	return false;
}




void Item::set_serialNumber(std::string serialNumber)
{
	if (serialNumber.length() != 5)//the length of the serial Number must be 5
	{
		throw (std::string("the serial number must be 5"));
	}
	//will go over the chars in the serial Number and if even one of the char are not number will do throw
	for (int i = 0; i < 5; i++)
	{
		if (serialNumber[i] < '0' || serialNumber[i] > '9')
		{
			throw (std::string("the serial number must be numbers"));
		}
	}
	this->_serialNumber = serialNumber;
}

void Item::set_unitPrice(double unitPrice)
{
	if (unitPrice <= 0)
	{
		throw (std::string("the unit price must be higher than 0"));
	}
	this->_unitPrice = unitPrice;
}

void Item::set_name(std::string name)
{
	this->_name = name;
}

void Item::set_count(int count)
{
	if (count < 1)
	{
		throw (std::string("the count must be 1 or higher"));
	}
	this->_count = count;
}

std::string Item::get_name() const
{
	return this->_name;
}

std::string Item::get_serialNumber() const
{
	return this->_serialNumber;
}

int Item::get_count() const
{
	return this->_count;
}

double Item::get_unitPrice() const
{
	return this->_unitPrice;
}
