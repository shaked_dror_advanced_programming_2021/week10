#pragma once
#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
	Item(std::string name, std::string serialNumber, double unitPrice);
	~Item();

	double totalPrice() const; //returns _count * _unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions
	void set_serialNumber(std::string serialNumber);
	void set_unitPrice(double unitPrice);
	void set_name(std::string name);
	void set_count(int count);

	std::string get_name() const;
	std::string get_serialNumber() const;
	int get_count() const;
	double get_unitPrice() const;
	


private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};